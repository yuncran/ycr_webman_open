<?php

namespace open\comm;

use open\controller\v1\IndexController;
use Webman\Route\Route;
use Webman\Route\Route as RouteObject;

/**
 * 分发地址
 * Class DishOut
 * @package open\comm
 * @author ycr <Email: yuncran@126.com>
 */
class DishOut
{

    /** 解析地址分配 */
    public function analysis($data)
    {
        $response = json_decode($data, true);

        /** 解析代码 */

        var_export(['data' => $response]);
    }


}