<?php

use Webman\Route;


/** ycr 对外开发放接口*/
Route::group('/v1/',function () {

    Route::post('open', [\open\controller\v1\IndexController::class, 'index'])->name('测试');
    Route::post('test', [\open\controller\v1\IndexController::class, 'test'])->name('测试2');

})->middleware(\app\middleware\CorsMiddleware::class);

