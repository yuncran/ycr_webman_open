<?php
declare(strict_types=1);

namespace open\controller\v1;

use open\controller\OpenApi;
use support\Request;

class IndexController extends OpenApi
{

    protected $app_id = '150';
    protected $app_secret = 'ycr';

    public function index(Request $request)
    {
        $url = $request = $request->uri(); #  '/v1/open?app_id=150&app_secret=ycr';
        $str = substr($url, strrpos($url, '?') + 1); # app_id=150&app_secret=ycr
        $array_str = explode('&', $str);
        $result = array();
        foreach ($array_str as $key => $val) {
            $str_response = explode('=', $val);
            $result[$str_response[0]] = $str_response[1];
        }
        return app('json')->success($result);
    }


    public function test()
    {
        return '我测试呐';
        return app('json')->success('我测试呐');
    }


    public function tst()
    {
        return '分发到了这个接口';
    }
}