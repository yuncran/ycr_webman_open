<?php

/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Webman\Route;

// 给所有OPTIONS请求设置跨域
//Route::options('[{path:.+}]', function (){
//    return response('');
//});

//Route::disableDefaultRoute();
/** 可能会放公共路由路由 */


Route::group(function () {

    Route::any('getAddress', [\app\controller\AddressController::class, 'getAddress'])->name('获取地址');


    Route::group(function () {

        Route::post('upload', [\app\controller\UploadController::class, 'upload'])->name('图片上传');
        Route::post('avatar', [\app\controller\UploadController::class, 'avatar'])->name('头像上传');

    })->middleware(['api'=>\app\api\middleware\AuthTokenMiddleware::class]);


})->middleware(\app\middleware\CorsMiddleware::class);