<?php

return [

    //是否开启限速设置---目前为访问IP限流(节流)---目前设置的在60秒时间内300
    'httpRateLimit' => false,
    // 签名验证默认关闭验证，如果开启需了解签名生成及验证
    'httpSignValidity' => false,
    // 签名授权公钥秘钥
    'httpSignAccount' => [
        'ycr' => 'e3de3825',
    ],
    // TODO 授权是否开启 true 开启
    'ycr_app' => true,
    // TODO 授权密钥
    'ycr_key' => '7126e9b5c2b41d575d82ab2706eb889a',

];