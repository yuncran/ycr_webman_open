<?php

namespace common\enums;


/**
 * 统一管理缓存键
 * Class CacheEnum
 * @package common\enums
 * @author ycr <Email: yuncran@126.com>
 */
class CacheEnum
{

    /** @var int 系统缓存时间 */
    const APP_CACHE_TIME = 7200;

    /** @var int  */
    const TIME = 3600;

    #---系统信息-------------------------------------------------------------------------------------------------------
    /** @var string 公用参数 */
    const APP_CONFIG = 'app-config-';

    /** @var string 商户code码 */
    const APP_MERCHANT_CODE = 'app-merchant-code-';

    /** @var string  省市区 */
    const APP_ADDRESS = 'app-address-';

    /** @var string  ip黑名单 */
    const APP_IP_BLACKLIST = 'app-ipBlacklist-';

    /** @var string 需要被记录的行为 */
    const APP_ACTION_BEHAVIOR = 'app-actionBehavior-';

    /** @var string 首页广告位 */
    const APP_ADV_LIST_ = 'app-adv-list-';

    /** @var string 首页推荐商品 */
    const APP_HOME_LIST = 'app-home-list-';



 #--------------------------------------------------------------------------------------------------------------------

 #---用户信息----------------------------------------------------------------------------------------------------------
    /** @var string 用户信息 */
    const APP_USER = 'app-user-';
}