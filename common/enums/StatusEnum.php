<?php

namespace common\enums;


/**
 * code 状态码
 * Class StatusEnum
 * @package common\enums
 * @author ycr <Email: yuncran@126.com>
 */
class StatusEnum extends BaseEnum
{

    const ENABLED = 1;
    const DISABLED = 0;
    const DELETE = -1;

    /**
     * @return array
     */
    public static function getMap(): array
    {
        return [
            self::ENABLED => '启用',
            self::DISABLED => '禁用',
        ];
    }

}