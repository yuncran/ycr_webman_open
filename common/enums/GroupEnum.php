<?php

namespace common\enums;

/**
 * Class GroupEnum
 * @package common\enums
 * @author ycr <Email: yuncran@126.com>
 */
class GroupEnum extends BaseEnum
{
    const DEFAULT = 'default';
    const PC = 'pc';
    const IOS = 'iOS';
    const ANDROID = 'android';
    const APP = 'app';
    const H5 = 'h5';
    const WE_CHAT = 'weChat';
    const WE_CHAT_MP = 'weChatMp';
    const ALI_MP = 'aliMp';
    const QQ_MP = 'qqMp';
    const BAI_DU_MP = 'baiDuMp';
    const DING_TALK_MP = 'dingTalkMp';
    const TOU_TIAO_MP = 'touTiaoMp';
    const WEB_SOCKET = 'webSocket';

    /**
     * @return array
     * @author ycr <Email: yuncran@126.com>
     */
    public static function getMap(): array
    {
        return [
            self::DEFAULT => '默认',
            self::IOS => 'iOS',
            self::ANDROID => 'Android',
            self::APP => 'App',
            self::H5 => 'H5',
            self::PC => 'PC',
            self::WE_CHAT => '微信',
            self::WE_CHAT_MP => '微信小程序',
            self::ALI_MP => '支付宝小程序',
            self::QQ_MP => 'QQ小程序',
            self::BAI_DU_MP => '百度小程序',
            self::DING_TALK_MP => '钉钉小程序',
            self::TOU_TIAO_MP => '头条小程序',
            self::WEB_SOCKET => 'WebSocket',
        ];
    }

}