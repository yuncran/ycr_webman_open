<?php

namespace common\validate\api;


use Tinywan\Validate\Validate;

class UserValidate extends Validate
{

    protected $rule = [
        'username' => '',
        'password_hash' => '',

    ];

    /**
     * @var array
     */
    protected $message = [
        'username' => '填写',
    ];

    protected $scene = [
        'save' => ['username', 'password_hash'],
    ];

}