<?php

namespace common\validate\common;


use Tinywan\Validate\Validate;

/**
 * Class SignAuthValidate
 * @package common\validate\common
 * @author ycr <Email: yuncran@126.com>
 */
class SignAuthValidate extends Validate
{

    /**
     * 时间过期容差
     *
     * @var int
     */
    public $toleranceTime = 60;

    /**
     * @var array
     */
    protected $rule = [
        'appId' => 'require',
        'time' => 'require|integer|isTime',
        'sign' => 'require|max:120',
        'nonceStr' => 'require|max:32'
    ];


    /**
     * @var array
     */
    protected $message = [
        'appId.require' => 'appId 必须填写',
        'appId.max' => 'appId 必须唯一',
        'time.require' => 'time 必须填写',
        'time.integer' => 'time 必须数字类型',
        'sign.require' => 'sign 必须填写',
        'sign.max' => 'sign 最多不能超过120个字符',
        'nonceStr.require' => 'nonceStr 必须填写',
        'nonceStr.max' => 'nonceStr 最多不能超过32个字符',
    ];


    /**
     * @param $attribute
     * @return array|bool
     * @author ycr <Email: yuncran@126.com>
     */
    public function isTime($attribute)
    {
        if (time() - $this->toleranceTime > $attribute) {
            return $this->parseErrorMsg('时间已过期', $attribute, '');
        }
        return true;
    }


}