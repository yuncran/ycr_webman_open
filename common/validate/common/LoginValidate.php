<?php

namespace common\validate\common;

use common\enums\GroupEnum;
use Tinywan\Validate\Validate;

/**
 * Class LoginValidate
 * @package common\validate\api
 * @author ycr <Email: yuncran@126.com>
 */
class LoginValidate extends Validate
{

    /**
     * @var array
     */
    protected $rule = [
        'username' => 'require',
        'password_hash' => 'require',
        'group' => ['require', 'isGroup'],
    ];

    /**
     * @var array
     */
    protected $message = [
        'username.require' => '登录账号必须填写',
        'password_hash.require' => '登录密码必须填写',
        'group.require' => '登录组别必须填写',
    ];

    protected $scene = [
        'login' => ['username', 'password_hash', 'group'],
        'register' => ['username', 'password_hash'],
    ];


    /**
     * @param $attribute
     * @return array|bool
     * @author ycr <Email: yuncran@126.com>
     */
    protected function isGroup($attribute)
    {
        if (!in_array($attribute, GroupEnum::getKeys())) {
            return $this->parseErrorMsg('请填写正确的范围', $attribute, '');
        }
        return true;
    }
}