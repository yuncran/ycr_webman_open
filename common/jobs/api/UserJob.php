<?php

namespace common\jobs\api;

use support\Log;

/**
 * 用户队列
 * Class UserJobs
 * @package common\jobs\api
 * @author ycr <Email: yuncran@126.com>
 */
class UserJob
{


    /**
     * 处理消费信息
     * @param $data
     * @author ycr <Email: yuncran@126.com>
     */
    public static function expense($data)
    {
        try {
            var_export(['name' => 'ycr', 'data' => $data]);
        } catch (\Throwable $exception) {
            /** 入库错误队列 记的把错误队列名称加进去喽*/
            Log::info('队列加入失败！',['job_name'=>'user-job']);
        }
    }
}