<?php

namespace common\jobs;

/**
 * 队列基类 -- 统一管理队列
 * Class BaseJobs
 * @package common\jobs
 * @author ycr <Email: yuncran@126.com>
 */
class AddJobs
{

    /**
     * 你的队列名称
     * @var array
     */
    public static $sendMail = ['name'=>'加入发邮件队列','value'=>'send-mail-job'];

    public static $sendUser = ['name'=>'加入发用户信息队列','value'=>'user-job'];









    /**
     * 投递消息
     *
     * @param $queue
     * @param $data
     * @author ycr <Email: yuncran@126.com>
     */
    public static function addQueueJobs($queue,$data)
    {
        return  \Webman\RedisQueue\Redis::send($queue, $data);
    }


}