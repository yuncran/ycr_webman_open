<?php

namespace common\traits;


use support\Request;

/**
 * Trait BaseAction
 * @package common\traits
 */
trait BaseAction
{

    /**
     * @var Request 实例
     */
    protected $request;

    /**
     * 商户 $merchant_id
     * @var int
     */
    protected $merchant_id = 0;

    /**
     * 默认分页
     *
     * @var int
     */
    protected $pageSize = 10;

    /**
     * 商户id
     * @return int
     */
    public function getMerchantId()
    {
        return $this->merchant_id;
    }


    /**
     * 重载配置
     * @author ycr <Email: yuncran@126.com>
     */
    public function __construct()
    {

//        $this->merchant_id = 6;
//        echo '我的乖乖了里' . PHP_EOL;

        // 微信配置 具体可参考EasyWechat

        // 微信支付配置 具体可参考EasyWechat

        // 微信小程序配置 具体可参考EasyWechat

        // 微信开放平台第三方平台配置 具体可参考EasyWechat

        // 微信企业微信配置 具体可参考EasyWechat

        // 微信企业微信开放平台 具体可参考EasyWechat

    }

    /**
     * 验证权限
     * @return bool
     * @author ycr <Email: yuncran@126.com>
     */
    protected function authCheck()
    {
        return true;
    }


    /**
     * 检测当前控制器和方法是否匹配传递的数组
     *
     * @param array $arr 需要验证权限的数组
     * @return boolean
     */
    public function match($arr = [])
    {

        $arr = is_array($arr) ? $arr : explode(',', $arr);

        if (!$arr) return false;

        $arr = array_map('strtolower', $arr);
        // 是否存在
        if (in_array(strtolower($this->request->action), $arr) || in_array('*', $arr)) {
            return true;
        }
        // 没找到匹配
        return false;
    }
}