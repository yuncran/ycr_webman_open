/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : webman_admin

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2023-05-22 22:21:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wa_admins
-- ----------------------------
DROP TABLE IF EXISTS `wa_admins`;
CREATE TABLE `wa_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(32) NOT NULL COMMENT '用户名',
  `nickname` varchar(40) NOT NULL COMMENT '昵称',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `avatar` varchar(255) DEFAULT '/app/admin/avatar.png' COMMENT '头像',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(16) DEFAULT NULL COMMENT '手机',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `login_at` datetime DEFAULT NULL COMMENT '登录时间',
  `status` tinyint(4) DEFAULT NULL COMMENT '禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Records of wa_admins
-- ----------------------------
INSERT INTO `wa_admins` VALUES ('1', 'admin', '超级管理员', '$2y$10$jntIzCqFMjFEExF0TW1JIeRS4hkC0/UfqiP2OwHoO0SeTvCQGMbnO', '/app/admin/avatar.png', null, null, '2023-05-07 18:22:55', '2023-05-07 18:35:53', '2023-05-07 18:35:53', null);
INSERT INTO `wa_admins` VALUES ('2', 'admin2', '哈哈哈', '$2y$10$GZJw72F/sf4OjWtEpC5vVeo7c9vJ9A7KqovwBz9r6iLHlYjugm3ZW', '/app/admin/avatar.png', 'yuncran@126.com', '', '2023-05-07 18:38:26', '2023-05-07 18:38:32', null, '0');

-- ----------------------------
-- Table structure for wa_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `wa_admin_roles`;
CREATE TABLE `wa_admin_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `admin_id` int(11) NOT NULL COMMENT '管理员id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_admin_id` (`role_id`,`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='管理员角色表';

-- ----------------------------
-- Records of wa_admin_roles
-- ----------------------------
INSERT INTO `wa_admin_roles` VALUES ('1', '1', '1');
INSERT INTO `wa_admin_roles` VALUES ('2', '2', '2');

-- ----------------------------
-- Table structure for wa_member
-- ----------------------------
DROP TABLE IF EXISTS `wa_member`;
CREATE TABLE `wa_member` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(10) unsigned DEFAULT '0' COMMENT '商户id',
  `username` varchar(20) DEFAULT '' COMMENT '帐号',
  `password_hash` varchar(150) DEFAULT '' COMMENT '密码',
  `auth_key` varchar(32) DEFAULT '' COMMENT '授权令牌',
  `password_reset_token` varchar(150) DEFAULT '' COMMENT '密码重置令牌',
  `type` tinyint(1) DEFAULT '1' COMMENT '类别[1:普通会员;10管理员]',
  `nickname` varchar(100) DEFAULT '' COMMENT '昵称',
  `realname` varchar(100) DEFAULT '' COMMENT '真实姓名',
  `head_portrait` varchar(150) DEFAULT '' COMMENT '头像',
  `current_level` tinyint(4) DEFAULT '1' COMMENT '当前级别',
  `gender` tinyint(1) unsigned DEFAULT '0' COMMENT '性别[0:未知;1:男;2:女]',
  `qq` varchar(20) DEFAULT '' COMMENT 'qq',
  `email` varchar(60) DEFAULT '' COMMENT '邮箱',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `visit_count` int(10) unsigned DEFAULT '1' COMMENT '访问次数',
  `home_phone` varchar(20) DEFAULT '' COMMENT '家庭号码',
  `mobile` varchar(20) DEFAULT '' COMMENT '手机号码',
  `role` smallint(6) DEFAULT '10' COMMENT '权限',
  `last_time` int(10) DEFAULT '0' COMMENT '最后一次登录时间',
  `last_ip` varchar(16) DEFAULT '' COMMENT '最后一次登录ip',
  `province_id` int(10) DEFAULT '0' COMMENT '省',
  `city_id` int(10) DEFAULT '0' COMMENT '城市',
  `area_id` int(10) DEFAULT '0' COMMENT '地区',
  `pid` int(10) unsigned DEFAULT '0' COMMENT '上级id',
  `level` tinyint(4) unsigned DEFAULT '1' COMMENT '级别',
  `promo_code` varchar(50) DEFAULT '' COMMENT '推广码',
  `tree` varchar(750) DEFAULT '' COMMENT '树',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[-1:删除;0:禁用;1启用]',
  `created_at` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(10) unsigned DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`uid`) USING BTREE,
  KEY `username` (`username`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE,
  KEY `tree` (`tree`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='用户_会员表';

-- ----------------------------
-- Records of wa_member
-- ----------------------------
INSERT INTO `wa_member` VALUES ('1', '0', 'admin', '123456', '123456', '', '1', '', '', '', '1', '0', '', '123@126.com', null, '14', '', '', '10', '1667563376', '127.0.0.1', '0', '0', '0', '0', '1', '4xp9rA1XLm', 'tr_0 ', '1', '1659772547', '1667563376');
INSERT INTO `wa_member` VALUES ('2', '0', '1111', '123456', '', '', '1', '', '', '', '1', '0', '', '', null, '1', '', '', '10', '0', '', '0', '0', '0', '0', '1', '', '', '1', '0', '0');
INSERT INTO `wa_member` VALUES ('3', '0', 'test', '123456', '', '', '1', '', '', '', '1', '0', '', '', null, '1', '', '', '10', '0', '', '0', '0', '0', '0', '1', '', '', '1', '0', '0');
INSERT INTO `wa_member` VALUES ('4', '0', '1111', '123456', '', '', '1', '', '', '', '1', '0', '', '', null, '1', '', '', '10', '0', '', '0', '0', '0', '0', '1', '', '', '1', '0', '0');
INSERT INTO `wa_member` VALUES ('5', '0', 'test', '123456', '', '', '1', '', '', '', '1', '0', '', '', null, '1', '', '', '10', '0', '', '0', '0', '0', '0', '1', '', '', '1', '0', '0');
INSERT INTO `wa_member` VALUES ('6', '0', 'test2', '123456', '', '', '1', '', '', '', '1', '0', '', '', null, '1', '', '', '10', '0', '', '0', '0', '0', '0', '1', '', '', '1', '0', '0');

-- ----------------------------
-- Table structure for wa_options
-- ----------------------------
DROP TABLE IF EXISTS `wa_options`;
CREATE TABLE `wa_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '键',
  `value` longtext NOT NULL COMMENT '值',
  `created_at` datetime NOT NULL DEFAULT '2022-08-15 00:00:00' COMMENT '创建时间',
  `updated_at` datetime NOT NULL DEFAULT '2022-08-15 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='选项表';

-- ----------------------------
-- Records of wa_options
-- ----------------------------
INSERT INTO `wa_options` VALUES ('1', 'system_config', '{\"logo\":{\"title\":\"Webman Admin\",\"image\":\"\\/app\\/admin\\/admin\\/images\\/logo.png\"},\"menu\":{\"data\":\"\\/app\\/admin\\/rule\\/get\",\"method\":\"GET\",\"accordion\":true,\"collapse\":false,\"control\":false,\"controlWidth\":500,\"select\":\"0\",\"async\":true},\"tab\":{\"enable\":true,\"keepState\":true,\"preload\":false,\"session\":true,\"max\":\"30\",\"index\":{\"id\":\"0\",\"href\":\"\\/app\\/admin\\/index\\/dashboard\",\"title\":\"\\u4eea\\u8868\\u76d8\"}},\"theme\":{\"defaultColor\":\"2\",\"defaultMenu\":\"light-theme\",\"defaultHeader\":\"light-theme\",\"allowCustom\":true,\"banner\":false},\"colors\":[{\"id\":\"1\",\"color\":\"#36b368\",\"second\":\"#f0f9eb\"},{\"id\":\"2\",\"color\":\"#2d8cf0\",\"second\":\"#ecf5ff\"},{\"id\":\"3\",\"color\":\"#f6ad55\",\"second\":\"#fdf6ec\"},{\"id\":\"4\",\"color\":\"#f56c6c\",\"second\":\"#fef0f0\"},{\"id\":\"5\",\"color\":\"#3963bc\",\"second\":\"#ecf5ff\"}],\"other\":{\"keepLoad\":\"500\",\"autoHead\":false,\"footer\":false},\"header\":{\"message\":false}}', '2022-12-05 14:49:01', '2022-12-08 20:20:28');
INSERT INTO `wa_options` VALUES ('2', 'table_form_schema_wa_users', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"主键\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"enable_sort\":true,\"searchable\":true,\"search_type\":\"normal\",\"form_show\":false},\"username\":{\"field\":\"username\",\"_field_id\":\"1\",\"comment\":\"用户名\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"nickname\":{\"field\":\"nickname\",\"_field_id\":\"2\",\"comment\":\"昵称\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"password\":{\"field\":\"password\",\"_field_id\":\"3\",\"comment\":\"密码\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"sex\":{\"field\":\"sex\",\"_field_id\":\"4\",\"comment\":\"性别\",\"control\":\"select\",\"control_args\":\"url:\\/app\\/admin\\/dict\\/get\\/sex\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"avatar\":{\"field\":\"avatar\",\"_field_id\":\"5\",\"comment\":\"头像\",\"control\":\"uploadImage\",\"control_args\":\"url:\\/app\\/admin\\/upload\\/avatar\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"email\":{\"field\":\"email\",\"_field_id\":\"6\",\"comment\":\"邮箱\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"mobile\":{\"field\":\"mobile\",\"_field_id\":\"7\",\"comment\":\"手机\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"level\":{\"field\":\"level\",\"_field_id\":\"8\",\"comment\":\"等级\",\"control\":\"inputNumber\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false},\"birthday\":{\"field\":\"birthday\",\"_field_id\":\"9\",\"comment\":\"生日\",\"control\":\"datePicker\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"between\",\"list_show\":false,\"enable_sort\":false},\"money\":{\"field\":\"money\",\"_field_id\":\"10\",\"comment\":\"余额(元)\",\"control\":\"inputNumber\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false},\"score\":{\"field\":\"score\",\"_field_id\":\"11\",\"comment\":\"积分\",\"control\":\"inputNumber\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false},\"last_time\":{\"field\":\"last_time\",\"_field_id\":\"12\",\"comment\":\"登录时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"between\",\"list_show\":false,\"enable_sort\":false},\"last_ip\":{\"field\":\"last_ip\",\"_field_id\":\"13\",\"comment\":\"登录ip\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false},\"join_time\":{\"field\":\"join_time\",\"_field_id\":\"14\",\"comment\":\"注册时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"between\",\"list_show\":false,\"enable_sort\":false},\"join_ip\":{\"field\":\"join_ip\",\"_field_id\":\"15\",\"comment\":\"注册ip\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false},\"token\":{\"field\":\"token\",\"_field_id\":\"16\",\"comment\":\"token\",\"control\":\"input\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"created_at\":{\"field\":\"created_at\",\"_field_id\":\"17\",\"comment\":\"创建时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"search_type\":\"between\",\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"updated_at\":{\"field\":\"updated_at\",\"_field_id\":\"18\",\"comment\":\"更新时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"search_type\":\"between\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"role\":{\"field\":\"role\",\"_field_id\":\"19\",\"comment\":\"角色\",\"control\":\"inputNumber\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"status\":{\"field\":\"status\",\"_field_id\":\"20\",\"comment\":\"禁用\",\"control\":\"switch\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-23 15:28:13');
INSERT INTO `wa_options` VALUES ('3', 'table_form_schema_wa_roles', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"主键\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"name\":{\"field\":\"name\",\"_field_id\":\"1\",\"comment\":\"角色组\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"rules\":{\"field\":\"rules\",\"_field_id\":\"2\",\"comment\":\"权限\",\"control\":\"treeSelectMulti\",\"control_args\":\"url:\\/app\\/admin\\/rule\\/get?type=0,1,2\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"created_at\":{\"field\":\"created_at\",\"_field_id\":\"3\",\"comment\":\"创建时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"updated_at\":{\"field\":\"updated_at\",\"_field_id\":\"4\",\"comment\":\"更新时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"pid\":{\"field\":\"pid\",\"_field_id\":\"5\",\"comment\":\"父级\",\"control\":\"select\",\"control_args\":\"url:\\/app\\/admin\\/role\\/select?format=tree\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-19 14:24:25');
INSERT INTO `wa_options` VALUES ('4', 'table_form_schema_wa_rules', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"主键\",\"control\":\"inputNumber\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"title\":{\"field\":\"title\",\"_field_id\":\"1\",\"comment\":\"标题\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"icon\":{\"field\":\"icon\",\"_field_id\":\"2\",\"comment\":\"图标\",\"control\":\"iconPicker\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"key\":{\"field\":\"key\",\"_field_id\":\"3\",\"comment\":\"标识\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"pid\":{\"field\":\"pid\",\"_field_id\":\"4\",\"comment\":\"上级菜单\",\"control\":\"treeSelect\",\"control_args\":\"\\/app\\/admin\\/rule\\/select?format=tree&type=0,1\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"created_at\":{\"field\":\"created_at\",\"_field_id\":\"5\",\"comment\":\"创建时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"updated_at\":{\"field\":\"updated_at\",\"_field_id\":\"6\",\"comment\":\"更新时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"href\":{\"field\":\"href\",\"_field_id\":\"7\",\"comment\":\"url\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"type\":{\"field\":\"type\",\"_field_id\":\"8\",\"comment\":\"类型\",\"control\":\"select\",\"control_args\":\"data:0:目录,1:菜单,2:权限\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"weight\":{\"field\":\"weight\",\"_field_id\":\"9\",\"comment\":\"排序\",\"control\":\"inputNumber\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-08 11:44:45');
INSERT INTO `wa_options` VALUES ('5', 'table_form_schema_wa_admins', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"ID\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"enable_sort\":true,\"search_type\":\"between\",\"form_show\":false,\"searchable\":false},\"username\":{\"field\":\"username\",\"_field_id\":\"1\",\"comment\":\"用户名\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"nickname\":{\"field\":\"nickname\",\"_field_id\":\"2\",\"comment\":\"昵称\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"password\":{\"field\":\"password\",\"_field_id\":\"3\",\"comment\":\"密码\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"avatar\":{\"field\":\"avatar\",\"_field_id\":\"4\",\"comment\":\"头像\",\"control\":\"uploadImage\",\"control_args\":\"url:\\/app\\/admin\\/upload\\/avatar\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"email\":{\"field\":\"email\",\"_field_id\":\"5\",\"comment\":\"邮箱\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"mobile\":{\"field\":\"mobile\",\"_field_id\":\"6\",\"comment\":\"手机\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"created_at\":{\"field\":\"created_at\",\"_field_id\":\"7\",\"comment\":\"创建时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"searchable\":true,\"search_type\":\"between\",\"list_show\":false,\"enable_sort\":false},\"updated_at\":{\"field\":\"updated_at\",\"_field_id\":\"8\",\"comment\":\"更新时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"search_type\":\"normal\",\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"login_at\":{\"field\":\"login_at\",\"_field_id\":\"9\",\"comment\":\"登录时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"between\",\"enable_sort\":false,\"searchable\":false},\"status\":{\"field\":\"status\",\"_field_id\":\"10\",\"comment\":\"禁用\",\"control\":\"switch\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-23 15:36:48');
INSERT INTO `wa_options` VALUES ('6', 'table_form_schema_wa_options', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"name\":{\"field\":\"name\",\"_field_id\":\"1\",\"comment\":\"键\",\"control\":\"input\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"value\":{\"field\":\"value\",\"_field_id\":\"2\",\"comment\":\"值\",\"control\":\"textArea\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"created_at\":{\"field\":\"created_at\",\"_field_id\":\"3\",\"comment\":\"创建时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"updated_at\":{\"field\":\"updated_at\",\"_field_id\":\"4\",\"comment\":\"更新时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-08 11:36:57');
INSERT INTO `wa_options` VALUES ('7', 'table_form_schema_wa_uploads', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"主键\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"enable_sort\":true,\"search_type\":\"normal\",\"form_show\":false,\"searchable\":false},\"name\":{\"field\":\"name\",\"_field_id\":\"1\",\"comment\":\"名称\",\"control\":\"input\",\"control_args\":\"\",\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false},\"url\":{\"field\":\"url\",\"_field_id\":\"2\",\"comment\":\"文件\",\"control\":\"upload\",\"control_args\":\"url:\\/app\\/admin\\/upload\\/file\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"admin_id\":{\"field\":\"admin_id\",\"_field_id\":\"3\",\"comment\":\"管理员\",\"control\":\"select\",\"control_args\":\"url:\\/app\\/admin\\/admin\\/select?format=select\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"file_size\":{\"field\":\"file_size\",\"_field_id\":\"4\",\"comment\":\"文件大小\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"between\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"mime_type\":{\"field\":\"mime_type\",\"_field_id\":\"5\",\"comment\":\"mime类型\",\"control\":\"input\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"image_width\":{\"field\":\"image_width\",\"_field_id\":\"6\",\"comment\":\"图片宽度\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"image_height\":{\"field\":\"image_height\",\"_field_id\":\"7\",\"comment\":\"图片高度\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"ext\":{\"field\":\"ext\",\"_field_id\":\"8\",\"comment\":\"扩展名\",\"control\":\"input\",\"control_args\":\"\",\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false},\"storage\":{\"field\":\"storage\",\"_field_id\":\"9\",\"comment\":\"存储位置\",\"control\":\"input\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"created_at\":{\"field\":\"created_at\",\"_field_id\":\"10\",\"comment\":\"上传时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"searchable\":true,\"search_type\":\"between\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false},\"category\":{\"field\":\"category\",\"_field_id\":\"11\",\"comment\":\"类别\",\"control\":\"select\",\"control_args\":\"url:\\/app\\/admin\\/dict\\/get\\/upload\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"updated_at\":{\"field\":\"updated_at\",\"_field_id\":\"12\",\"comment\":\"更新时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-08 11:47:45');
INSERT INTO `wa_options` VALUES ('8', 'table_form_schema_wa_uploads', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"主键\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"enable_sort\":true,\"search_type\":\"normal\",\"form_show\":false,\"searchable\":false},\"name\":{\"field\":\"name\",\"_field_id\":\"1\",\"comment\":\"名称\",\"control\":\"input\",\"control_args\":\"\",\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false},\"url\":{\"field\":\"url\",\"_field_id\":\"2\",\"comment\":\"文件\",\"control\":\"upload\",\"control_args\":\"url:\\/app\\/admin\\/upload\\/file\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"admin_id\":{\"field\":\"admin_id\",\"_field_id\":\"3\",\"comment\":\"管理员\",\"control\":\"select\",\"control_args\":\"url:\\/app\\/admin\\/admin\\/select?format=select\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"file_size\":{\"field\":\"file_size\",\"_field_id\":\"4\",\"comment\":\"文件大小\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"between\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"mime_type\":{\"field\":\"mime_type\",\"_field_id\":\"5\",\"comment\":\"mime类型\",\"control\":\"input\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"image_width\":{\"field\":\"image_width\",\"_field_id\":\"6\",\"comment\":\"图片宽度\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"image_height\":{\"field\":\"image_height\",\"_field_id\":\"7\",\"comment\":\"图片高度\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false,\"searchable\":false},\"ext\":{\"field\":\"ext\",\"_field_id\":\"8\",\"comment\":\"扩展名\",\"control\":\"input\",\"control_args\":\"\",\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"form_show\":false,\"enable_sort\":false},\"storage\":{\"field\":\"storage\",\"_field_id\":\"9\",\"comment\":\"存储位置\",\"control\":\"input\",\"control_args\":\"\",\"search_type\":\"normal\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false,\"searchable\":false},\"created_at\":{\"field\":\"created_at\",\"_field_id\":\"10\",\"comment\":\"上传时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"searchable\":true,\"search_type\":\"between\",\"form_show\":false,\"list_show\":false,\"enable_sort\":false},\"category\":{\"field\":\"category\",\"_field_id\":\"11\",\"comment\":\"类别\",\"control\":\"select\",\"control_args\":\"url:\\/app\\/admin\\/dict\\/get\\/upload\",\"form_show\":true,\"list_show\":true,\"searchable\":true,\"search_type\":\"normal\",\"enable_sort\":false},\"updated_at\":{\"field\":\"updated_at\",\"_field_id\":\"12\",\"comment\":\"更新时间\",\"control\":\"dateTimePicker\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-08 11:47:45');
INSERT INTO `wa_options` VALUES ('9', 'dict_upload', '[{\"value\":\"1\",\"name\":\"分类1\"},{\"value\":\"2\",\"name\":\"分类2\"},{\"value\":\"3\",\"name\":\"分类3\"}]', '2022-12-04 16:24:13', '2022-12-04 16:24:13');
INSERT INTO `wa_options` VALUES ('10', 'dict_sex', '[{\"value\":\"0\",\"name\":\"女\"},{\"value\":\"1\",\"name\":\"男\"}]', '2022-12-04 15:04:40', '2022-12-04 15:04:40');
INSERT INTO `wa_options` VALUES ('11', 'dict_status', '[{\"value\":\"0\",\"name\":\"正常\"},{\"value\":\"1\",\"name\":\"禁用\"}]', '2022-12-04 15:05:09', '2022-12-04 15:05:09');
INSERT INTO `wa_options` VALUES ('17', 'table_form_schema_wa_admin_roles', '{\"id\":{\"field\":\"id\",\"_field_id\":\"0\",\"comment\":\"主键\",\"control\":\"inputNumber\",\"control_args\":\"\",\"list_show\":true,\"enable_sort\":true,\"searchable\":true,\"search_type\":\"normal\",\"form_show\":false},\"role_id\":{\"field\":\"role_id\",\"_field_id\":\"1\",\"comment\":\"角色id\",\"control\":\"inputNumber\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false},\"admin_id\":{\"field\":\"admin_id\",\"_field_id\":\"2\",\"comment\":\"管理员id\",\"control\":\"inputNumber\",\"control_args\":\"\",\"form_show\":true,\"list_show\":true,\"search_type\":\"normal\",\"enable_sort\":false,\"searchable\":false}}', '2022-08-15 00:00:00', '2022-12-20 19:42:51');

-- ----------------------------
-- Table structure for wa_roles
-- ----------------------------
DROP TABLE IF EXISTS `wa_roles`;
CREATE TABLE `wa_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(80) NOT NULL COMMENT '角色组',
  `rules` text COMMENT '权限',
  `created_at` datetime NOT NULL COMMENT '创建时间',
  `updated_at` datetime NOT NULL COMMENT '更新时间',
  `pid` int(10) unsigned DEFAULT NULL COMMENT '父级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='管理员角色';

-- ----------------------------
-- Records of wa_roles
-- ----------------------------
INSERT INTO `wa_roles` VALUES ('1', '超级管理员', '*', '2022-08-13 16:15:01', '2022-12-23 12:05:07', null);
INSERT INTO `wa_roles` VALUES ('2', '测试', '85,86,87,88,115,116', '2023-05-07 18:37:55', '2023-05-07 18:37:55', '1');

-- ----------------------------
-- Table structure for wa_rules
-- ----------------------------
DROP TABLE IF EXISTS `wa_rules`;
CREATE TABLE `wa_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `key` varchar(255) NOT NULL COMMENT '标识',
  `pid` int(10) unsigned DEFAULT '0' COMMENT '上级菜单',
  `created_at` datetime NOT NULL COMMENT '创建时间',
  `updated_at` datetime NOT NULL COMMENT '更新时间',
  `href` varchar(255) DEFAULT NULL COMMENT 'url',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '类型',
  `weight` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COMMENT='权限规则';

-- ----------------------------
-- Records of wa_rules
-- ----------------------------
INSERT INTO `wa_rules` VALUES ('1', '数据库', 'layui-icon-template-1', 'database', '0', '2023-05-07 18:22:42', '2023-05-07 18:22:42', null, '0', '1000');
INSERT INTO `wa_rules` VALUES ('2', '所有表', null, 'plugin\\admin\\app\\controller\\TableController', '1', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/table/index', '1', '800');
INSERT INTO `wa_rules` VALUES ('3', '权限管理', 'layui-icon-vercode', 'auth', '0', '2023-05-07 18:22:42', '2023-05-07 18:22:42', null, '0', '900');
INSERT INTO `wa_rules` VALUES ('4', '账户管理', null, 'plugin\\admin\\app\\controller\\AdminController', '3', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/admin/index', '1', '1000');
INSERT INTO `wa_rules` VALUES ('5', '角色管理', null, 'plugin\\admin\\app\\controller\\RoleController', '3', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/role/index', '1', '900');
INSERT INTO `wa_rules` VALUES ('6', '菜单管理', null, 'plugin\\admin\\app\\controller\\RuleController', '3', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/rule/index', '1', '800');
INSERT INTO `wa_rules` VALUES ('7', '会员管理', 'layui-icon-username', 'user', '0', '2023-05-07 18:22:42', '2023-05-07 18:22:42', null, '0', '800');
INSERT INTO `wa_rules` VALUES ('8', '用户', null, 'plugin\\admin\\app\\controller\\UserController', '7', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/user/index', '1', '800');
INSERT INTO `wa_rules` VALUES ('9', '通用设置', 'layui-icon-set', 'common', '0', '2023-05-07 18:22:42', '2023-05-07 18:22:42', null, '0', '700');
INSERT INTO `wa_rules` VALUES ('10', '个人资料', null, 'plugin\\admin\\app\\controller\\AccountController', '9', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/account/index', '1', '800');
INSERT INTO `wa_rules` VALUES ('11', '附件管理', null, 'plugin\\admin\\app\\controller\\UploadController', '9', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/upload/index', '1', '700');
INSERT INTO `wa_rules` VALUES ('12', '字典设置', null, 'plugin\\admin\\app\\controller\\DictController', '9', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/dict/index', '1', '600');
INSERT INTO `wa_rules` VALUES ('13', '系统设置', null, 'plugin\\admin\\app\\controller\\ConfigController', '9', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/config/index', '1', '500');
INSERT INTO `wa_rules` VALUES ('14', '插件管理', 'layui-icon-app', 'plugin', '0', '2023-05-07 18:22:42', '2023-05-07 18:22:42', null, '0', '600');
INSERT INTO `wa_rules` VALUES ('15', '应用插件', null, 'plugin\\admin\\app\\controller\\PluginController', '14', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/plugin/index', '1', '800');
INSERT INTO `wa_rules` VALUES ('16', '开发辅助', 'layui-icon-fonts-code', 'dev', '0', '2023-05-07 18:22:42', '2023-05-07 18:22:42', null, '0', '500');
INSERT INTO `wa_rules` VALUES ('17', '表单构建', null, 'plugin\\admin\\app\\controller\\DevController', '16', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/dev/form-build', '1', '800');
INSERT INTO `wa_rules` VALUES ('18', '示例页面', 'layui-icon-templeate-1', 'demos', '0', '2023-05-07 18:22:42', '2023-05-07 18:22:42', null, '0', '400');
INSERT INTO `wa_rules` VALUES ('19', '工作空间', 'layui-icon-console', 'demo1', '18', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '', '0', '0');
INSERT INTO `wa_rules` VALUES ('20', '控制后台', 'layui-icon-console', 'demo10', '19', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/console/console1.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('21', '数据分析', 'layui-icon-console', 'demo13', '19', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/console/console2.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('22', '百度一下', 'layui-icon-console', 'demo14', '19', '2023-05-07 18:22:42', '2023-05-07 18:22:42', 'http://www.baidu.com', '1', '0');
INSERT INTO `wa_rules` VALUES ('23', '主题预览', 'layui-icon-console', 'demo15', '19', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/theme.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('24', '常用组件', 'layui-icon-component', 'demo20', '18', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '', '0', '0');
INSERT INTO `wa_rules` VALUES ('25', '功能按钮', 'layui-icon-face-smile', 'demo2011', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/button.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('26', '表单集合', 'layui-icon-face-cry', 'demo2014', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/form.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('27', '字体图标', 'layui-icon-face-cry', 'demo2010', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/icon.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('28', '多选下拉', 'layui-icon-face-cry', 'demo2012', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/select.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('29', '动态标签', 'layui-icon-face-cry', 'demo2013', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/tag.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('30', '数据表格', 'layui-icon-face-cry', 'demo2031', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/table.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('31', '分布表单', 'layui-icon-face-cry', 'demo2032', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/step.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('32', '树形表格', 'layui-icon-face-cry', 'demo2033', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/treetable.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('33', '树状结构', 'layui-icon-face-cry', 'demo2034', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/dtree.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('34', '文本编辑', 'layui-icon-face-cry', 'demo2035', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/tinymce.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('35', '卡片组件', 'layui-icon-face-cry', 'demo2036', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/card.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('36', '抽屉组件', 'layui-icon-face-cry', 'demo2021', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/drawer.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('37', '消息通知', 'layui-icon-face-cry', 'demo2022', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/notice.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('38', '加载组件', 'layui-icon-face-cry', 'demo2024', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/loading.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('39', '弹层组件', 'layui-icon-face-cry', 'demo2023', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/popup.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('40', '多选项卡', 'layui-icon-face-cry', 'demo60131', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/tab.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('41', '数据菜单', 'layui-icon-face-cry', 'demo60132', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/menu.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('42', '哈希加密', 'layui-icon-face-cry', 'demo2041', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/encrypt.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('43', '图标选择', 'layui-icon-face-cry', 'demo2042', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/iconPicker.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('44', '省市级联', 'layui-icon-face-cry', 'demo2043', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/area.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('45', '数字滚动', 'layui-icon-face-cry', 'demo2044', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/count.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('46', '顶部返回', 'layui-icon-face-cry', 'demo2045', '24', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/document/topBar.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('47', '结果页面', 'layui-icon-auz', 'demo666', '18', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '', '0', '0');
INSERT INTO `wa_rules` VALUES ('48', '成功', 'layui-icon-face-smile', 'demo667', '47', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/result/success.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('49', '失败', 'layui-icon-face-cry', 'demo668', '47', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/result/error.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('50', '错误页面', 'layui-icon-face-cry', 'demo-error', '18', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '', '0', '0');
INSERT INTO `wa_rules` VALUES ('51', '403', 'layui-icon-face-smile', 'demo403', '50', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/error/403.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('52', '404', 'layui-icon-face-cry', 'demo404', '50', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/error/404.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('53', '500', 'layui-icon-face-cry', 'demo500', '50', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/error/500.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('54', '系统管理', 'layui-icon-set-fill', 'demo-system', '18', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '', '0', '0');
INSERT INTO `wa_rules` VALUES ('55', '用户管理', 'layui-icon-face-smile', 'demo601', '54', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/user.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('56', '角色管理', 'layui-icon-face-cry', 'demo602', '54', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/role.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('57', '权限管理', 'layui-icon-face-cry', 'demo603', '54', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/power.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('58', '部门管理', 'layui-icon-face-cry', 'demo604', '54', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/deptment.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('59', '行为日志', 'layui-icon-face-cry', 'demo605', '54', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/log.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('60', '数据字典', 'layui-icon-face-cry', 'demo606', '54', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/dict.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('61', '常用页面', 'layui-icon-template-1', 'demo-common', '18', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '', '0', '0');
INSERT INTO `wa_rules` VALUES ('62', '空白页面', 'layui-icon-face-smile', 'demo702', '61', '2023-05-07 18:22:42', '2023-05-07 18:22:42', '/app/admin/demos/system/space.html', '1', '0');
INSERT INTO `wa_rules` VALUES ('63', '查看表', null, 'plugin\\admin\\app\\controller\\TableController@view', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('64', '查询表', null, 'plugin\\admin\\app\\controller\\TableController@show', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('65', '创建表', null, 'plugin\\admin\\app\\controller\\TableController@create', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('66', '修改表', null, 'plugin\\admin\\app\\controller\\TableController@modify', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('67', '一键菜单', null, 'plugin\\admin\\app\\controller\\TableController@crud', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('68', '查询记录', null, 'plugin\\admin\\app\\controller\\TableController@select', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('69', '插入记录', null, 'plugin\\admin\\app\\controller\\TableController@insert', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('70', '更新记录', null, 'plugin\\admin\\app\\controller\\TableController@update', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('71', '删除记录', null, 'plugin\\admin\\app\\controller\\TableController@delete', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('72', '删除表', null, 'plugin\\admin\\app\\controller\\TableController@drop', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('73', '表摘要', null, 'plugin\\admin\\app\\controller\\TableController@schema', '2', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('74', '插入', null, 'plugin\\admin\\app\\controller\\AdminController@insert', '4', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('75', '更新', null, 'plugin\\admin\\app\\controller\\AdminController@update', '4', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('76', '删除', null, 'plugin\\admin\\app\\controller\\AdminController@delete', '4', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('77', '插入', null, 'plugin\\admin\\app\\controller\\RoleController@insert', '5', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('78', '更新', null, 'plugin\\admin\\app\\controller\\RoleController@update', '5', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('79', '删除', null, 'plugin\\admin\\app\\controller\\RoleController@delete', '5', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('80', '获取角色权限', null, 'plugin\\admin\\app\\controller\\RoleController@rules', '5', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('81', '查询', null, 'plugin\\admin\\app\\controller\\RuleController@select', '6', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('82', '添加', null, 'plugin\\admin\\app\\controller\\RuleController@insert', '6', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('83', '更新', null, 'plugin\\admin\\app\\controller\\RuleController@update', '6', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('84', '删除', null, 'plugin\\admin\\app\\controller\\RuleController@delete', '6', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('85', '插入', null, 'plugin\\admin\\app\\controller\\UserController@insert', '8', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('86', '更新', null, 'plugin\\admin\\app\\controller\\UserController@update', '8', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('87', '查询', null, 'plugin\\admin\\app\\controller\\UserController@select', '8', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('88', '删除', null, 'plugin\\admin\\app\\controller\\UserController@delete', '8', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('89', '更新', null, 'plugin\\admin\\app\\controller\\AccountController@update', '10', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('90', '修改密码', null, 'plugin\\admin\\app\\controller\\AccountController@password', '10', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('91', '查询', null, 'plugin\\admin\\app\\controller\\AccountController@select', '10', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('92', '添加', null, 'plugin\\admin\\app\\controller\\AccountController@insert', '10', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('93', '删除', null, 'plugin\\admin\\app\\controller\\AccountController@delete', '10', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('94', '浏览附件', null, 'plugin\\admin\\app\\controller\\UploadController@attachment', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('95', '查询附件', null, 'plugin\\admin\\app\\controller\\UploadController@select', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('96', '更新附件', null, 'plugin\\admin\\app\\controller\\UploadController@update', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('97', '添加附件', null, 'plugin\\admin\\app\\controller\\UploadController@insert', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('98', '上传文件', null, 'plugin\\admin\\app\\controller\\UploadController@file', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('99', '上传图片', null, 'plugin\\admin\\app\\controller\\UploadController@image', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('100', '上传头像', null, 'plugin\\admin\\app\\controller\\UploadController@avatar', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('101', '删除附件', null, 'plugin\\admin\\app\\controller\\UploadController@delete', '11', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('102', '查询', null, 'plugin\\admin\\app\\controller\\DictController@select', '12', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('103', '插入', null, 'plugin\\admin\\app\\controller\\DictController@insert', '12', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('104', '更新', null, 'plugin\\admin\\app\\controller\\DictController@update', '12', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('105', '删除', null, 'plugin\\admin\\app\\controller\\DictController@delete', '12', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('106', '更改', null, 'plugin\\admin\\app\\controller\\ConfigController@update', '13', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('107', '列表', null, 'plugin\\admin\\app\\controller\\PluginController@list', '15', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('108', '安装', null, 'plugin\\admin\\app\\controller\\PluginController@install', '15', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('109', '卸载', null, 'plugin\\admin\\app\\controller\\PluginController@uninstall', '15', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('110', '支付', null, 'plugin\\admin\\app\\controller\\PluginController@pay', '15', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('111', '登录官网', null, 'plugin\\admin\\app\\controller\\PluginController@login', '15', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('112', '获取已安装的插件列表', null, 'plugin\\admin\\app\\controller\\PluginController@getInstalledPlugins', '15', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('113', '表单构建', null, 'plugin\\admin\\app\\controller\\DevController@formBuild', '17', '2023-05-07 18:23:52', '2023-05-07 18:23:52', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('114', '注册设置', null, 'plugin\\user\\app\\admin\\controller\\RegisterController', '7', '2023-05-07 18:33:24', '2023-05-07 18:33:24', '/app/user/admin/register', '1', '0');
INSERT INTO `wa_rules` VALUES ('115', '保存设置', null, 'plugin\\user\\app\\admin\\controller\\RegisterController@saveSetting', '114', '2023-05-07 18:37:31', '2023-05-07 18:37:31', null, '2', '0');
INSERT INTO `wa_rules` VALUES ('116', '获取配置', null, 'plugin\\user\\app\\admin\\controller\\RegisterController@getSetting', '114', '2023-05-07 18:37:31', '2023-05-07 18:37:31', null, '2', '0');

-- ----------------------------
-- Table structure for wa_uploads
-- ----------------------------
DROP TABLE IF EXISTS `wa_uploads`;
CREATE TABLE `wa_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(128) NOT NULL COMMENT '名称',
  `url` varchar(255) NOT NULL COMMENT '文件',
  `admin_id` int(11) DEFAULT NULL COMMENT '管理员',
  `file_size` int(11) NOT NULL COMMENT '文件大小',
  `mime_type` varchar(255) NOT NULL COMMENT 'mime类型',
  `image_width` int(11) DEFAULT NULL COMMENT '图片宽度',
  `image_height` int(11) DEFAULT NULL COMMENT '图片高度',
  `ext` varchar(128) NOT NULL COMMENT '扩展名',
  `storage` varchar(255) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `created_at` date DEFAULT NULL COMMENT '上传时间',
  `category` varchar(128) DEFAULT NULL COMMENT '类别',
  `updated_at` date DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  KEY `admin_id` (`admin_id`),
  KEY `name` (`name`),
  KEY `ext` (`ext`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='附件';

-- ----------------------------
-- Records of wa_uploads
-- ----------------------------

-- ----------------------------
-- Table structure for wa_users
-- ----------------------------
DROP TABLE IF EXISTS `wa_users`;
CREATE TABLE `wa_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(32) NOT NULL COMMENT '用户名',
  `nickname` varchar(40) NOT NULL COMMENT '昵称',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `sex` enum('0','1') NOT NULL DEFAULT '1' COMMENT '性别',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(16) DEFAULT NULL COMMENT '手机',
  `level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '等级',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '余额(元)',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `last_time` datetime DEFAULT NULL COMMENT '登录时间',
  `last_ip` varchar(50) DEFAULT NULL COMMENT '登录ip',
  `join_time` datetime DEFAULT NULL COMMENT '注册时间',
  `join_ip` varchar(50) DEFAULT NULL COMMENT '注册ip',
  `token` varchar(50) DEFAULT NULL COMMENT 'token',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `role` int(11) NOT NULL DEFAULT '1' COMMENT '角色',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `join_time` (`join_time`),
  KEY `mobile` (`mobile`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of wa_users
-- ----------------------------
INSERT INTO `wa_users` VALUES ('1', 'user', '哈哈哈', '$2y$10$xgls2Mbv7OaVA62gJruYgOdNt2wdlBAWNWg/cTGqxz6Oheql0Epda', '1', '/app/user/default-avatar.png', 'yuncran@126.com', '15000000000', '0', null, '0.00', '0', null, null, null, null, null, '2023-05-07 18:35:24', '2023-05-07 18:37:08', '1', '0');
