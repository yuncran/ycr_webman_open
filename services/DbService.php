<?php

namespace services;

use common\interfaces\DbInterface;
use think\facade\Db;

/**
 * Class DbService
 * @package services
 *
 * @package think\facade\Db $db
 *
 * @author ycr <Email: yuncran@126.com>
 */
class DbService
{

    /**
     * @var string
     */
    private $db = Db::class;

    /** 开启事务 */
    public function startTrans()
    {
        return $this->db::startTrans();
    }

    /** 提交事务 */
    public function commit()
    {
        return $this->db::commit();
    }

    /** 回滚事务 */
    public function rollback()
    {
        return $this->db::rollback();
    }

}