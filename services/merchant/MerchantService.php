<?php

namespace services\merchant;

use app\model\merchant\MerchantModel;
use common\enums\CacheEnum;

/**
 * Class MerchantService
 * @package services\merchant
 * @author ycr <Email: yuncran@126.com>
 */
class MerchantService
{

    /**
     * @var int
     */
    protected $merchant_id = 0;

    /**
     * 获取商户 ID
     * @return int
     */
    public function getMerchantId()
    {
        return $this->merchant_id;
    }

    /**
     * 设置商户 ID
     * @param $merchant_id
     */
    protected function setMerchantId($merchant_id)
    {
        $this->merchant_id = $merchant_id;
    }

    /** 获取商户信息 */
    public function getMerchant(string $code)
    {
        $redis = Yii()->cache;
        $key = CacheEnum::APP_MERCHANT_CODE . $code;
        $merchant = $redis::get($key);
        if (!$merchant) {
            $merchant = MerchantModel::where(['merchant_code' => $code])->find();
            $merchant_id = empty($merchant) ? 0 : $merchant;
            $redis::set($key, $merchant_id, 7200);
        }
        return $merchant;
    }

    public function test(){
        return 'ssss';
    }

}