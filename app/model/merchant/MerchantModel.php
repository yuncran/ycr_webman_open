<?php

namespace app\model\merchant;


use app\model\BaseThinkModel;

/**
 * Class MerchantModel
 * @package app\model\merchant
 * @author ycr <Email: yuncran@126.com>
 */
class MerchantModel extends BaseThinkModel
{
    // 表名
    protected $name = 'wa_merchant';

    protected $pk = 'merchant_id';

    // 定义时间戳字段名
//    protected $updateTime = 'update_time';

    protected $hidden = ['created_time','update_time'];

}