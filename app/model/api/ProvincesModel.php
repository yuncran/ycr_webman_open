<?php

namespace app\model\api;


use app\model\BaseThinkModel;

class ProvincesModel extends BaseThinkModel
{
    // 表名
    protected $name = 'wa_provinces';

    protected $pk = 'id';

    // 定义时间戳字段名
//    protected $updateTime = 'update_time';

    protected $hidden = ['created_at','updated_at'];


    public static function getAddress(int $pid)
    {
       return self::getModel()->where(['pid'=>$pid])->field(['id','title','pid','level','status'])->select();
    }
}