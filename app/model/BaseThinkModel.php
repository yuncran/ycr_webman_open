<?php


namespace app\model;


use think\Model;

class BaseThinkModel extends Model
{

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    /**
     * 定义时间戳字段名
     * @var string
     */
    protected $createTime = 'create_time';


}