<?php

namespace app\controller;

use support\Request;

class IndexController
{

    public function index(Request $request)
    {
        return response('hello 大家好 我是 ycr --- 作者@Email : yuncran@126.com');
    }

    public function view(Request $request)
    {
        return view('index/view', ['name' => 'webman']);
    }

    public function json(Request $request)
    {
        return json(['code' => 0, 'msg' => 'ok']);
    }

}
