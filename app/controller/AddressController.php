<?php

namespace app\controller;

use app\controller\api\BaseController;
use app\model\api\ProvincesModel;
use common\enums\CacheEnum;
use support\Request;

/**
 * 省市区
 * Class AddressController
 * @package app\controller
 * @author ycr <Email: yuncran@126.com>
 */
class AddressController extends BaseController
{

    /**
     * 省市区地址
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @author ycr <Email: yuncran@126.com>
     */
    public function getAddress(Request $request)
    {
        $pid = $request->post('pid') > 0 ? $request->post('pid') : 1;
        $key = CacheEnum::APP_ADDRESS . $pid;
        $redis = Yii()->cache;
        if (!$data = $redis::get($key)) {
            $data = ProvincesModel::getAddress($pid);
            $redis::set($key, $data, 7200);
        }
        return app('json')->success($data);
    }
}