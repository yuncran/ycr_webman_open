<?php

namespace app\middleware;

use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;

class MerchantMiddleware implements MiddlewareInterface
{
    public function process(Request $request, callable $next): Response
    {
        /** 判断是否存在该商户 默认为平台*/
        $merchantCode = $request->header('merchant_code', 0);
        $merchantInfo = Yii()->merchant->getMerchant($merchantCode);
        /** 强制开启商户验证 */
        if (empty($merchantInfo) && 0 != $merchantInfo) {
            return app('json')->fail('该商户不存再');
        }
        /** 判断状态 如果状态为 0 则提示该商户以及配关停或者禁用 */
        return $next($request);
    }

}
