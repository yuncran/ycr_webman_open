<?php

use Webman\Route;

Route::any('/index', [\app\api\controller\IndexController::class, 'index'])->name('测试');
/** ycr */
Route::group('/api', function () {

    /** 无需验证登录状态 */
    Route::get('/captcha', [\app\api\controller\SiteController::class, 'captcha'])->name('验证码');
    Route::post('/login', [\app\api\controller\SiteController::class, 'login'])->name('用户登录');
    Route::post('/register', [\app\api\controller\SiteController::class, 'register'])->name('用户注册');

    /** 需要验证登录状态 开始 */
    Route::group('/', function () {
        Route::post('refreshToken', [\app\api\controller\SiteController::class, 'refreshToken'])->name('重置令牌');
        Route::post('logout', [\app\api\controller\SiteController::class, 'logout'])->name('退出登录');
    })->middleware([\app\api\middleware\AuthTokenMiddleware::class]);

    /** v1 版本路由 */
    Route::group('/v1/', function () {
        /** 首页信息 */
        Route::group('home/',function () {
            Route::post('getHomeConfig', [\app\api\controller\v1\home\homeController::class, 'getHomeConfig'])->name('首页配置信息');
            Route::post('getHomeAdvList', [\app\api\controller\v1\home\homeController::class, 'getHomeAdvList'])->name('首页广告位');
            Route::post('getHomeList', [\app\api\controller\v1\home\homeController::class, 'getHomeList'])->name('首页列表');
        });

        /** ------------------------------------------------------------------------------------------------------------------------ */
        /** 需要验证登录状态 开始 */
        Route::group(function () {
            Route::post('user/getUser', [\app\api\controller\v1\user\UserController::class, 'getUser'])->name('获取用户信息');
            Route::post('user/userSave', [\app\api\controller\v1\user\UserController::class, 'userSave'])->name('修改用户信息');
        })->middleware([\app\api\middleware\AuthTokenMiddleware::class]);

    });

    /** v2 版本路由 */
    Route::group('/v2', function () {

    });


})->middleware(\app\middleware\CorsMiddleware::class);

