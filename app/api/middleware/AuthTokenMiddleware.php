<?php

namespace app\api\middleware;


use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;


/**
 * 用户在线
 * Class AuthTokenMiddleware
 * @package app\middleware\api
 * @author ycr <Email: yuncran@126.com>
 */
class AuthTokenMiddleware implements MiddlewareInterface
{

    public function process(Request $request, callable $next): Response
    {
        return $next($request);
    }

}
