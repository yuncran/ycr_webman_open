<?php

namespace app\api\controller\v1\home;

use app\controller\api\BaseController;
use app\logic\api\HomeLogic;
use support\Container;
use support\Request;

/**
 * 首页信息
 * Class homeController
 * @package app\api\controller\v1\home
 * @author ycr <Email: yuncran@126.com>
 */
class homeController extends BaseController
{

    /**
     * 签名验证 -白名单
     * @var array
     */
    protected $signOptional = [
//        '/api/v1/home/getHomeList',
    ];

    /**
     * 权限验证 -白名单
     * @var array
     */
    protected $authOptional = [];

    /**
     * @var HomeLogic homeLogic
     */
    public $homeLogic;

    public function __construct()
    {
        $this->homeLogic = Container::get(HomeLogic::class);
    }


    /**
     * 加载首页配置信息
     * @author ycr <Email: yuncran@126.com>
     */
    public function getHomeConfig()
    {
        $data = [
            'user_info' => false,
            'is_user' => false,
        ];
        return app('json')->success($data);
    }

    /**
     * 加载首页广告位信息
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author ycr <Email: yuncran@126.com>
     */
    public function getHomeAdvList()
    {
        $merchant_id = Yii()->merchant->getMerchantId();
        return app('json')->success($this->homeLogic->homeBannerList($merchant_id));
    }

    /** 推荐商品列表 */
    public function getHomeList(Request $request)
    {
        $page = $request->get('page') < 0 ? 1 : $request->get('page');
        $search = $request->post();
        return app('json')->success($this->homeLogic->getHomeList($search, $page));
    }

}