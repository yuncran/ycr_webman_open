<?php

namespace app\api\controller\v1;


use app\controller\api\BaseController;
use support\Request;


class IndexController extends BaseController
{

    /**
     * 签名验证 -- 白名单
     * @var array
     */
    protected $signOptional = [];

    /**
     * 权限验证 -- 白名单
     * @var array
     */
    protected $authOptional = [];

    /**
     * 测试接口
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @author ycr <Email: yuncran@126.com>
     */

    public function index3(Request $request)
    {
        $data = ['测试成功---需要权限',      $request->uid()];
        return app('json')->success($data);
    }

    public function test()
    {
        $data = ['测试方法二'];
        return app('json')->success($data);
    }

}