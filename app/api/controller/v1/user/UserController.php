<?php

namespace app\api\controller\v1\user;

use app\controller\api\BaseController;
use app\logic\api\UserLogic;
use common\validate\api\UserValidate;
use support\Container;
use support\Request;

/**
 * Class UserController
 * @package app\api\controller\v1
 * @author ycr <Email: yuncran@126.com>
 */
class UserController extends BaseController
{
    /**
     * 签名验证 -- 白名单
     * @var array
     */
    protected $signOptional = [];

    /**
     * 权限验证 -- 白名单
     * @var array
     */
    protected $authOptional = [];


    /**
     *
     * @var UserLogic userLogic
     */
    protected $userLogic;


    public function __construct()
    {
        return $this->userLogic = Container::get(UserLogic::class);
    }

    /** 获取用户信息 */
    public function getUser(Request $request)
    {
        $uid = $request->uid();
        return app('json')->success($this->userLogic->getUserInfo($uid));
    }

    /** 修改用户信息 */
    public function userSave(Request $request)
    {
        $uid = $request->uid();
        $param = $request->post();
        $validate = new UserValidate();
        if (!$validate->scene('save')->check($param)) {
            return app('json')->fail($validate->getError());
        }
        return app('json')->success($this->userLogic->userSave($uid, $param));
    }

}