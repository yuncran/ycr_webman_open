<?php

namespace app\api\controller\v2;


use app\controller\api\BaseController;

class TestController extends BaseController
{
    /**
     * 签名验证 -- 白名单
     * @var array
     */
    protected $signOptional = [];

    /**
     * 权限验证 -- 白名单
     * @var array
     */
    protected $authOptional = [];


    public function test()
    {
        return json('这是我的测试信息');
    }

}