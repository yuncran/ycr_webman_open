<?php

namespace app\api\models\think;

use app\model\BaseThinkModel;

/**
 * 广告位模型
 * Class AdvModel
 * @package app\api\models\think
 * @author ycr <Email: yuncran@126.com>
 */
class AdvModel extends BaseThinkModel
{
    // 表名
    protected $name = 'wa_article_adv';

    protected $pk = 'id';

    // 定义时间戳字段名
//    protected $updateTime = 'update_time';

    protected $hidden = ['sort','update_time'];

}