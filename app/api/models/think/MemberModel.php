<?php

namespace app\api\models\think;


use app\model\BaseThinkModel;

/**
 * 用户模型
 * Class MemberModel
 * @package app\api\models\think
 * @author ycr <Email: yuncran@126.com>
 */
class MemberModel extends BaseThinkModel
{
    // 表名
    protected $name = 'wa_member';


    protected $pk = 'uid';

    // 定义时间戳字段名
    protected $updateTime = 'update_time';


    /** @var array 查询隐藏 */
    protected $hidden = ['deleted_at'];

    // 追加属性
    protected $append = [
        'status_text',
    ];

    /**
     * @param array $where
     * @param array $field
     * @return array|mixed|\think\db\Query|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author ycr <Email: yuncran@126.com>
     */
    public static function getUser(array $where, array $field = ['*'])
    {
        return MemberModel::field($field)->where($where)->find();
    }

    /**
     * @param $value
     * @param $data
     * @return mixed
     * @author ycr <Email: yuncran@126.com>
     */
    public function getStatusTextAttr($value, $data)
    {
        $status = [-1 => '删除', 0 => '禁用', 1 => '正常'];
        if (isset($data['status'])) {
            return $status[$data['status']];
        }
    }

}