<?php

namespace app\api\models\think;


use app\model\BaseThinkModel;

class GoodsModel extends BaseThinkModel
{
    // 表名
    protected $name = 'wa_goods';

    protected $pk = 'id';

    // 定义时间戳字段名
//    protected $updateTime = 'update_time';

    protected $hidden = ['update_time'];


    public function searchStatusAttr($query, $value)
    {
        $query->where('status', $value);
    }


    public function searchTitleAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('title', 'like', $value . '%');
        }
    }

}