<?php

namespace app\queue\open;

use open\comm\DishOut;
use support\Container;
use Webman\RedisQueue\Consumer;

class OpenRouteQueue implements Consumer
{

    // 要消费的队列名
    public $queue = 'open_url';

    // 这里设置为default，代表消费配置里default为key的队列
//    public $connection = 'default';


    public function consume($data)
    {
        /** @var DishOut $class */
        $class = Container::get(DishOut::class);

        return $class->analysis($data);

//        var_export(['url' => json_decode($data, true), 'author' => 'ycr1']);
    }
}