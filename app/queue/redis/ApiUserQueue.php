<?php

namespace app\queue\redis;


use common\jobs\api\UserJob;
use Webman\RedisQueue\Consumer;

class ApiUserQueue implements Consumer
{
    // 要消费的队列名
    public $queue = 'send-mail-job';

    // 这里设置为default，代表消费配置里default为key的队列
    public $connection = 'default';


    /**
     * 消费
     * @param $data
     * @author ycr <Email: yuncran@126.com>
     */
    public function consume($data)
    {
        return UserJob::expense($data);
    }
}