<?php

namespace app\logic\api;


use app\api\models\think\MemberModel;
use Webman\Event\Event;

/**
 * 用户登录
 * Class LoginLogic
 * @package app\logic\api
 * @author ycr <Email: yuncran@126.com>
 */
class LoginLogic
{

    /**
     * 获取用户登录信息
     * @param $data
     * @return array|mixed|\think\db\Query|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author ycr <Email: yuncran@126.com>
     */
    public function siteLogin($data)
    {
        unset($data['group']);
        $user = MemberModel::getUser($data, ['*']);
        if ($user) {
            $token = Yii()->access_token->getAccessToken($user->uid);
            $user = array_merge($token, ['user' => $user]);
        }
//        Event::emit('api.user.login', $user);
        return $user;
    }

    /**
     * 注册服务
     * @param $data
     * @return array
     * @author ycr <Email: yuncran@126.com>
     */
    public function register($data)
    {
        $user = MemberModel::create($data);
        $token = Yii()->access_token->getAccessToken($user->uid);
        $response = array_merge($token, ['user' => $user]);
        return $response;
    }


}