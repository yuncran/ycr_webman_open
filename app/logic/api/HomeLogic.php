<?php

namespace app\logic\api;

use app\api\models\think\AdvModel;
use app\api\models\think\GoodsModel;
use cache\home\HomeCache;
use common\enums\CacheEnum;
use support\Container;

/**
 * 首页信息
 * Class HomeLogic
 * @package app\logic\api
 * @author ycr <Email: yuncran@126.com>
 */
class HomeLogic
{

    /**
     * @var HomeCache homeCache
     */
    public $homeCache;


    public function __construct()
    {
        $this->homeCache = Container::get(HomeCache::class);
    }

    /**
     * 获取广告位
     * @param int $merchant_id
     * @return array|\think\Collection|\think\db\Query[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author ycr <Email: yuncran@126.com>
     */
    public function homeBannerList(int $merchant_id)
    {
        if (!$response = $this->homeCache->getHomeBannerCache($merchant_id)) {
            $response = AdvModel::where(['merchant_id' => $merchant_id])->order(['sort' => 'DESC'])->select();
            $this->homeCache->setHomeBannerCache($merchant_id, json_encode($response, true));
        }
        return $response;
    }

    /**
     * 获取首页列表信息
     * @param $search
     * @param int $page
     * @return mixed
     * @author ycr <Email: yuncran@126.com>
     */
    public function getHomeList($search, int $page)
    {
        $title = isset($search['title']) ? $search['title'] : [];
        $wy = empty($title) ? '' : $title;
        $merchant_id = Yii()->merchant->getMerchantId();
        $key = CacheEnum::APP_HOME_LIST . $merchant_id . $page . $wy;
        if (!$response = Yii()->cache::get($key)) {
            $response = GoodsModel::getList($merchant_id, $title, $page);
            Yii()->cache::set($key, $response, 3600);
        }
        return $response;
    }

}