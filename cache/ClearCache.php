<?php

namespace cache;


use common\enums\UserEnum;

/**
 * 清理缓存
 * Class ClearCache
 * @package cache
 * @author ycr <Email: yuncran@126.com>
 */
class ClearCache
{


    /**
     * 选择标签清理缓存
     * @param int $merchant_id
     * @param string $tag
     * @return int
     * @author ycr <Email: yuncran@126.com>
     */
    public function choiceClearCache(int $merchant_id, string $tag)
    {
        $key = UserEnum::getPrefix($merchant_id, $tag);
        return Yii()->redis::del($key);
    }

}